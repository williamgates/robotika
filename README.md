# Mobile Robot - Bebas

## Part 1

```bash
$ rosrun m2wr roda_to_robot_and_pose.py --help
$ rosrun m2wr roda_to_robot_and_pose.py -x 0 -y 0 -psi 0 -r_w 5 -l_w 5 -t 5 -l 0.3 -r 0.1 -f roda_to_pose
```

```bash
$ roslaunch gazebo_ros empty_world.launch
$ roslaunch m2wr spawn.launch
$ rosrun m2wr eksperimen_robot_part_1.py
```

## Part 2

```bash
$ roslaunch m2wr circuit.launch
$ roslaunch m2wr spawn.launch
$ rosrun m2wr eksperimen_robot_part_2.py
```

## Part 3

```bash
$ roslaunch m2wr circuit.launch
$ roslaunch m2wr spawn.launch
$ rosrun m2wr eksperimen_robot_part_3.py
```