from __future__ import division, print_function

import rospy
from gazebo_msgs.srv import GetModelState
from geometry_msgs.msg import Twist

import utils


class M2WR:
    def __init__(self):
        self.topic = '/m2wr/cmd_vel'
        self.x = 0
        self.y = 0
        self.psi = 0
        self.l = 0.3
        self.r = 0.1

    def get_actual_coordinates(self):
        model_coordinates = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        coordinates = model_coordinates('m2wr', '')
        return coordinates

    def reinit_position(self):
        coordinates = self.get_actual_coordinates()
        position = coordinates.pose.position
        self.x = position.x
        self.y = position.y
        orientation = coordinates.pose.orientation
        euler = utils.get_euler_from_orientation(orientation)
        self.psi = euler[2]

    def move(self, lin_speed, ang_speed, time):
        velocity_publisher = rospy.Publisher(self.topic, Twist, queue_size=10)
        vel_msg = Twist()
        vel_msg.linear.x = lin_speed
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = ang_speed
        while not rospy.is_shutdown():
            t0 = rospy.Time.now().to_sec()
            while(rospy.Time.now().to_sec() - t0 < time):
                velocity_publisher.publish(vel_msg)
            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            velocity_publisher.publish(vel_msg)
            break
