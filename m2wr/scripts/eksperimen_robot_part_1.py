#!/usr/bin/env python
from __future__ import division, print_function

from signal import SIGINT, signal

import rospy

import roda_to_robot_and_pose
import utils
from robot import M2WR

if __name__ == '__main__':
    print('---Eksperimen Part 1---')
    signal(SIGINT, utils.handler)
    rospy.init_node('m2wr_controller', anonymous=True)
    robot = M2WR()
    try:
        count = 0
        while True:
            print('\n')
            print('---Sequence {}---'.format(count + 1))
            l_w = float(raw_input('Kecepatan sudut roda kiri\t\t: '))
            r_w = float(raw_input('Kecepatan sudut roda kanan\t\t: '))
            t = float(raw_input('Waktu\t\t\t\t\t: '))
            robot.reinit_position()
            utils.print_hr()
            print('CALCULATING LINEAR VELOCITY AND ANGULAR VELOCITY')
            utils.print_hr()
            [v, w] = roda_to_robot_and_pose.roda_to_robot(robot.x, robot.y, robot.psi, l_w, r_w, t, robot.l, robot.r)
            utils.print_hr()
            print('FINAL POSITION PREDICTION')
            utils.print_hr()
            [x, y, psi] = roda_to_robot_and_pose.roda_to_pose(robot.x, robot.y, robot.psi, l_w, r_w, t, robot.l, robot.r, offset=[-0.05, 0], is_gazebo=True)
            robot.move(v, w, t)
            coordinates = robot.get_actual_coordinates()
            utils.print_hr()
            print('ACTUAL POSITION')
            utils.print_hr()
            print('Posisi X\t\t\t\t: {}'.format(coordinates.pose.position.x))
            print('Posisi Y\t\t\t\t: {}'.format(coordinates.pose.position.y))
            print('Sudut akhir\t\t\t\t: {}rad'.format(utils.convert_rad(utils.get_euler_from_orientation(coordinates.pose.orientation)[2])))
            count += 1
    except rospy.ROSInterruptException: 
        pass
