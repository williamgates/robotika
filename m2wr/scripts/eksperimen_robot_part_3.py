#!/usr/bin/env python
from __future__ import division, print_function

from signal import SIGINT, signal

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

import roda_to_robot_and_pose
import utils
from robot import M2WR

history = []


def check_if_stuck(tol=0.01):
    global history

    if len(history) < 3:
        return False
    a, b, c = history[-1], history[-2], history[-3],
    ab, ac, bc = abs(a-b), abs(a-c), abs(b-c)
    
    if ab < tol and ac < tol and bc < tol:
        return True
    return False


def move_straight(duration, backward=False):
    if backward:
        return (-8, -8, duration)
    return (8, 8, duration)


def move_90_deg(direction, backward=False):
    if backward:
        print('Moving {} backward'.format(direction))
        w1, w2 = -9, -11
        if direction == 'right':
            return (w2, w1, 2)
        return (w1, w2, 2)
    else:
        print('Moving {}'.format(direction))
        w1, w2 = 16.4933, 21.2057
        if direction == 'left':
            return (w2, w1, 1)
        return (w1, w2, 1)


def move_robot(regions, robot):
    global history
    history.append(regions['front'])
    right_sum = regions['fright'] + regions['right']
    left_sum = regions['fleft'] + regions['left']
    if regions['front'] > 2.2:
        action = move_straight(1)
    elif (left_sum > right_sum) and (left_sum > 3):
        action = move_90_deg('left', backward=check_if_stuck()) #remove the backward bit to disable stuck escape mechanism
    elif (left_sum <= right_sum) and (right_sum > 3):
        action = move_90_deg('right', backward=check_if_stuck()) #remove the backward bit to disable stuck escape mechanism
    else:
        print('Hmm...')
        action = move_straight(1, backward=check_if_stuck()) #remove the backward bit to disable stuck escape mechanism
    l_w, r_w, t = float(action[0]), float(action[1]), float(action[2])
    [v, w] = roda_to_robot_and_pose.roda_to_robot(robot.x, robot.y, robot.psi, l_w, r_w, t, robot.l, robot.r, print_results=False)
    robot.move(v, w, t)
    rospy.sleep(0.1)


def laser_cb(msg, robot):
    regions = {
        'right':  min(min(msg.ranges[0:143]), 10),
        'fright': min(min(msg.ranges[144:287]), 10),
        'front':  min(min(msg.ranges[308:411]), 10),
        'fleft':  min(min(msg.ranges[432:575]), 10),
        'left':   min(min(msg.ranges[576:719]), 10),
    }
    print(regions)
    move_robot(regions, robot)


if __name__ == '__main__':
    print('---Eksperimen Part 3---')
    signal(SIGINT, utils.handler)
    rospy.init_node('m2wr_controller', anonymous=True)
    robot = M2WR()
    try:
        while not rospy.is_shutdown():
            print('\n')
            laser_cb(rospy.wait_for_message('/m2wr/laser/scan', LaserScan), robot)
    except rospy.ROSInterruptException:
        pass
