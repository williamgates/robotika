#!/usr/bin/env python
from __future__ import division, print_function

import math
from sys import exit

import tf


def convert_rad(rad):
    while rad < 0:
        rad += math.pi * 2
    while rad > math.pi * 2:
        rad -= math.pi * 2
    return rad


def handler(signal_received, frame):
    print()
    print('SIGINT or CTRL-C detected. Exiting gracefully.')
    exit(0)


def print_hr():
    print('--------------------------------------------------------------------')


def get_euler_from_orientation(orientation):
    return tf.transformations.euler_from_quaternion((orientation.x, orientation.y, orientation.z, orientation.w))
