#!/usr/bin/env python
from __future__ import division, print_function

import argparse
import sys

from std_msgs.msg import String
import numpy as np
import rospy

import utils


def float_is_equal(a, b):
    return np.isclose(a, b, rtol=1e-05, atol=1e-08, equal_nan=False)


def roda_to_robot(x, y, psi, l_w, r_w, t, l, r, print_results=True):
    delta = np.dot(
        np.array([[r / 2, r / 2], [0, 0], [-r / l, r / l]]),
        np.array([l_w, r_w])
    )
    [v, _, w] = delta

    if print_results:
        print('Kecepatan linear\t\t\t: {}'.format(v))
        print('Kecepatan sudut\t\t\t\t: {}'.format(w))

    return [v, w]


def roda_to_pose(x, y, psi, l_w, r_w, t, l, r, offset=[0, 0], print_results=True, is_gazebo=False):
    x -= offset[0]
    y -= offset[1]

    [v, w] = roda_to_robot(x, y, psi, l_w, r_w, t, l, r, print_results=False)

    if is_gazebo:
        w *= -1

    l_v = l_w * r
    r_v = r_w * r
    if float_is_equal(l_v, r_v):
        coordinates = np.array([v * np.cos(psi) * t + x, v * np.sin(psi) * t + y, psi])
    else:
        R = l * (l_v + r_v) / (2 * (r_v - l_v))
        icc = np.array([x - R * np.sin(psi), y + R * np.cos(psi)])
        coordinates = np.dot(
            np.array([[np.cos(w * t), -np.sin(w * t), 0], [np.sin(w * t), np.cos(w * t), 0], [0, 0, 1]]),
            np.array([x - icc[0], y - icc[1], psi])
        ) + np.array([icc[0], icc[1], w * t])
    [new_x, new_y, new_psi] = coordinates

    new_x += offset[0]
    new_y += offset[1]

    if print_results:
        print('Posisi akhir X\t\t\t\t: {}'.format(new_x))
        print('Posisi akhir Y\t\t\t\t: {}'.format(new_y))
        print('Sudut akhir\t\t\t\t: {}rad'.format(utils.convert_rad(new_psi)))

    return [new_x, new_y, new_psi]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--posisi_x', type=float, required=True)
    parser.add_argument('-y', '--posisi_y', type=float, required=True)
    parser.add_argument('-psi', '--sudut_robot', type=float, required=True)
    parser.add_argument('-r_w', '--kecepatan_sudut_roda_kanan', type=float, required=True)
    parser.add_argument('-l_w', '--kecepatan_sudut_roda_kiri', type=float, required=True)
    parser.add_argument('-t', '--durasi_gerak', type=float, required=True)
    parser.add_argument('-l', '--jarak_antar_roda', type=float, required=True)
    parser.add_argument('-r', '--jari_jari_roda', type=float, required=True)
    parser.add_argument('-f', '--fungsi', type=str, choices=['roda_to_robot', 'roda_to_pose'], required=True)
    args = parser.parse_args()

    x = args.posisi_x
    y = args.posisi_y
    psi = args.sudut_robot
    r_w = args.kecepatan_sudut_roda_kanan
    l_w = args.kecepatan_sudut_roda_kiri
    t = args.durasi_gerak
    l = args.jarak_antar_roda
    r = args.jari_jari_roda
    f = args.fungsi

    if f == 'roda_to_robot':
        roda_to_robot(x, y, psi, l_w, r_w, t, l, r)
    elif f == 'roda_to_pose':
        roda_to_pose(x, y, psi, l_w, r_w, t, l, r)
