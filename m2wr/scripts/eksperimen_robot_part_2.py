#!/usr/bin/env python
from __future__ import division, print_function

from signal import SIGINT, signal

import rospy

import roda_to_robot_and_pose
import utils
from robot import M2WR


def move_straight(duration):
    return (12, 12, duration)


def move_90_deg(direction):
    w1, w2 = 16.4933, 21.2057
    if direction == 'left':
        return (w2, w1, 1)
    return (w1, w2, 1)


if __name__ == '__main__':
    print('---Eksperimen Part 2---')
    signal(SIGINT, utils.handler)
    rospy.init_node('m2wr_controller', anonymous=True)
    robot = M2WR()
    try:
        sequences = [
            move_straight(1.15),
            move_90_deg('right'),
            move_straight(0.75),
            move_90_deg('left'),
            move_straight(2.25),
            move_90_deg('right'),
            move_straight(2.15),
            move_90_deg('right'),
            move_straight(0.75),
            move_90_deg('right'),
            move_90_deg('left'),
            move_straight(0.5),
            move_90_deg('left'),
            move_straight(1.5)
        ]

        for i in range(len(sequences)):
            rospy.sleep(0.1)
            robot.reinit_position()
            seq = sequences[i]
            print('\n')
            l_w, r_w, t = float(seq[0]), float(seq[1]), float(seq[2])
            print('---Sequence {}: l_w={}, r_w={}, t={}---'.format(i + 1, l_w, r_w, t))
            [v, w] = roda_to_robot_and_pose.roda_to_robot(robot.x, robot.y, robot.psi, l_w, r_w, t, robot.l, robot.r)
            robot.move(v, w, t)

        print('Finished')
    except rospy.ROSInterruptException:
        pass
